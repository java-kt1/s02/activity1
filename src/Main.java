import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner leapYear = new Scanner(System.in);

        System.out.println("Enter a year to be checked if it is a leap year: ");

        int year = leapYear.nextInt();

        if(year % 4 == 0){
            if(year % 100 == 0){
                if(year % 400 == 0){
                    System.out.println(year + " is a leap year");
                }else{
                    System.out.println(year + " is NOT a leap year");
                }
            }else{
                System.out.println(year + " is a leap year");
            }
        }else{
            System.out.println(year + " is NOT a leap year");
        }


    }
}